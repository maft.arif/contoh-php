<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>String PHP</title>
</head>
<body>
    <h1>Berlatih String PHP</h1>
    <?php   
        echo "<h3> Soal No 1</h3>";
        /* 
            SOAL NO 1
            Tunjukan dengan menggunakan echo berapa panjang dari string yang diberikan berikut! Tunjukkan juga jumlah kata di dalam kalimat 
tersebut! 

            Contoh: 
            $string = "PHP is never old";
            Output:
            Panjang string: 16, 
            Jumlah kata: 4 
        */

        $first_sentence = "Hello PHP!" ; // Panjang string 10, jumlah kata: 2
        $panjang = strlen($first_sentence);
        $kata = str_word_count($first_sentence);
        echo "<h4>$first_sentence</h4>";
        echo "Panjang string : <b>$panjang</b><br>";
        echo "Jumlah kata : <b>$kata</b><br>";

        $second_sentence = "I'm ready for the challenges"; // Panjang string: 28,  jumlah kata: 5
        $panjang2 = strlen($second_sentence);
        $kata2 = str_word_count($second_sentence);        
        echo "<h4>$second_sentence</h4>";
        echo "Panjang string : <b>$panjang2</b><br>";
        echo "Jumlah kata : <b>$kata2</b><br>";

        echo "<br>";
        echo "<hr>";

        echo "<h3> Soal No 2</h3>";
        /* 
            SOAL NO 2
            Mengambil kata pada string dan karakter-karakter yang ada di dalamnya. 
            
            
        */
        $string2 = "I love PHP";
        
        echo "<label>String: </label> \"<b>$string2</b>\" <br>";
        echo "Kata pertama: " . substr($string2, 0, 1) . "<br>" ; 
        // Lanjutkan di bawah ini
        echo "Kata kedua: " . substr($string2, 2, 4) . "<br>"; 
        echo "Kata Ketiga: " . substr($string2, 7, 3) . "<br>";

        echo "<br>";
        echo "<hr>";

        echo "<h3> Soal No 3 </h3>";
        /*
            SOAL NO 3
            Mengubah karakter atau kata yang ada di dalam sebuah string.
        */
        $string3 = "PHP is old but Good!";
        echo "String: \"$string3\" "; 
        $ubah = str_replace("Good", "Awesome", $string3);
        echo "<br>";
        echo "Ganti menjadi : <b>$ubah</b>";
        // OUTPUT : "PHP is old but awesome"

    ?>
</body>
</html>